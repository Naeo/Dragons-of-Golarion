# Description

This sub-repository contains a simple Python script to transliterate from
English into other scripts.  It's not perfect, but it's not meant to be--it's
just meant to be "sort of" close enough (if you squint).

Some of the scripts have had pretty generous liberties taken with their
sound inventories to better fit English, e.g., by taking letters for sounds that
to not exist in English and re-purposing them for sounds that do.  Additionally,
some liberties are taken with English pronunciation, e.g., merging many vowels
together.

The IPA_ENGLISH file is the eSpeak IPA output of Charles Dickens' "A Tale Of Two
Cities," pulled from Project Gutenberg (with front matter in tact).  It is used
for testing new scripts to make sure there is complete coverage of the English
phonological space in any given script.  It's also useful for generating large 
blocks of output text for demonstrations of what a given script looks like.

This project only requires Python 3.6+, with no additional dependencies.
The script `transliterator.py` transliterates arbitrary input text.  The script
`transliterator_linewise.py` transliterates text files using Groff-like macros
to change scripts mid-document.  Both programs are intended to be run from the
command line; run `python3 transliterator.py --help` for usage details.

**Please note that `transliterator_linewise.py` is out of sync with 
`transliterator.py` and will not necessarily support the same set of languages.**

Currently supported scripts:
- Avestan
- Elder Futhark (runes)
- Georgian
- Glagolitic
- Gurmukhi
- Inuktitut (augmented with some symbols from broader Canadian Aboriginal syllabics)
- Medeival Runes
- Mongolian
- Phagspa
- Yi

Of these, Medeival Runes is guaranteed to work as intended, since it is a simple
one-to-one letter-to-rune mapping.  It is thus also guaranteed to be easily
reversible.  The same is not necessarily true for all others, though it is
generally true for Elder Futhark.